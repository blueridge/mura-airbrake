### SUMMARY ###

Mura Airbrake is a simple Mura plugin that sends onGlobalError exception information to Airbrake or Errbit. This plugin utilizes AirbrakeNotifier.cfc developed and maintained by [Tim Blair](https://github.com/timblair).

---------------------------------------
### PROJECT NOTES ###

For use with Errbit:

Update the endpoints from the default Airbrake endpoints to your Errbit installation endpoints. They should look something like:

```
    <cfset variables.airbrake_endpoint = {
        default = "http://errbit.example.com/notifier_api/v2/notices/",
        secure  = "https://errbit.example.com/notifier_api/v2/notices/"
    }>
```
---------------------------------------
### USEFUL LINKS ###

https://github.com/timblair/coldfusion-airbrake-notifier