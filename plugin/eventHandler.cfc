component extends="mura.plugin.pluginGenericEventHandler" {

	public void function onApplicationLoad() {
		if(structKeyExists(application, "Airbrake")) {
			structDelete(application, "Airbrake");
		}
		variables.pluginConfig.addEventHandler(this);
	}
	
	public void function onGlobalError() {
		try {
			if(!structKeyExists(application, "Airbrake")) {
				// Instantiate Airbrake and set variables
				var _airBrake =  createObject("component", "Airbrake.AirbrakeNotifier").init( api_key=variables.pluginConfig.getSetting("APIKey") );
				_airBrake.setEnvironment( variables.pluginConfig.getSetting("Environment") );
				_airBrake.setUseSSL( variables.pluginConfig.getSetting("UseSSL") );

				// Set Airbrake in the application scope for future use
				application.Airbrake = _airBrake;
			}
			
			// Track Exception w/Airbrake
			application.Airbrake.send(arguments.$.event('exception'));
		} catch(any e){
			/* BEGIN: Make sure we get notified if Airbrake fails! */
			variables.emailObj = application.serviceFactory.getBean('mailer');
			emailArgs = {};
			emailArgs.sendTo = variables.pluginConfig.getSetting("FailoverEmail");
			emailArgs.siteID = arguments.$.event("siteID");
			emailArgs.subject = "[#uCase(emailArgs.siteID)#-AIRBRAKE] onGlobalError Airbrake Exception";
			savecontent variable="emailArgs.html" {
				writeDump( var=e, label='Airbrake Exception', top=10 );
				writeDump( var=arguments.$.event('exception'), label='Original Exception', top=10 );
				writeDump( var=CGI, label='CGI', top=10 );
				writeDump( var=request, label='Request', top=10 );
			}
			variables.emailObj.sendHTML(argumentCollection=emailArgs);
			/* END: Make sure we get notified if Airbrake fails! */
		}
		if( len(trim( variables.pluginConfig.getSetting("ExceptionURLRedirect") )) )location(url=variables.pluginConfig.getSetting("ExceptionURLRedirect"),addtoken=false);
	}
}