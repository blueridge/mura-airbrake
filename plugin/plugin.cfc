<cfcomponent output="false" extends="mura.plugin.plugincfc">

	<cfset variables.config = '' />

	<cffunction name="init" access="public" returntype="any" output="false">
		<cfargument name="config"  type="any" default="" />
		
		<cfset variables.config = arguments.config />
	</cffunction>
	
	<cffunction name="install" access="public" returntype="void" output="false">
		<cfset application.appInitialized = false />
		<cfif structKeyExists(application, "Airbrake")>
			<cfset structDelete(application, "Airbrake") />
		</cfif>
	</cffunction>

	<cffunction name="update" access="public" returntype="void" output="false">
		<cfset application.appInitialized = false />
		<cfif structKeyExists(application, "Airbrake")>
			<cfset structDelete(application, "Airbrake") />
		</cfif>
	</cffunction>
	
	<cffunction name="delete" access="public" returntype="void" output="false">
		<cfset application.appInitialized = false />
		<cfif structKeyExists(application, "Airbrake")>
			<cfset structDelete(application, "Airbrake") />
		</cfif>
	</cffunction>

</cfcomponent>